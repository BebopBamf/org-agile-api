{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell #-}

module Schema.Project (
    ProjectP (..),
    ProjectE,
    selectProject,
    selectProjectPage,
    selectProjectById,
    insertProject,
    updateProject,
    deleteProject,
) where

import RIO

import Data.Profunctor.Product.TH (makeAdaptorAndInstance)
import Data.Time (LocalTime)
import Data.UUID (UUID)
import Opaleye

data ProjectP a b c d = ProjectE
    { projectEId :: a
    , projectEName :: b
    , projectEDescription :: c
    , projectECreatedAt :: d
    }
    deriving (Eq, Show)

type ProjectE = ProjectP UUID Text Text LocalTime
type ProjectWrite = ProjectP (Maybe (Field SqlUuid)) (Field SqlVarcharN) (Maybe (Field SqlText)) (Maybe (Field SqlTimestamp))
type ProjectField = ProjectP (Field SqlUuid) (Field SqlVarcharN) (Field SqlText) (Field SqlTimestamp)

$(makeAdaptorAndInstance "pProject" ''ProjectP)

projectTable :: Table ProjectWrite ProjectField
projectTable =
    table
        "projects"
        ( pProject
            ProjectE
                { projectEId = tableField "id"
                , projectEName = tableField "name"
                , projectEDescription = tableField "description"
                , projectECreatedAt = tableField "created_at"
                }
        )

-- | Query Functions
selectProject :: Select ProjectField
selectProject = selectTable projectTable

selectProjectPage :: Int32 -> Int32 -> Select ProjectField
selectProjectPage lim off =
    limit (fromIntegral lim)
        $ offset (fromIntegral off)
        $ orderBy (desc projectECreatedAt) selectProject

selectProjectById :: UUID -> Select ProjectField
selectProjectById id_ = do
    rows <- selectProject
    where_ (projectEId rows .== toFields id_)

    pure rows

-- | Mutation Functions
insertProject :: Text -> Maybe Text -> Insert [UUID]
insertProject name description =
    Insert
        { iTable = projectTable
        , iRows =
            [ ProjectE
                { projectEId = Nothing
                , projectEName = toFields name
                , projectEDescription = toFields description
                , projectECreatedAt = Nothing
                }
            ]
        , iReturning = rReturning projectEId
        , iOnConflict = Nothing
        }

updateProject :: UUID -> Text -> Maybe Text -> Maybe LocalTime -> Update [LocalTime]
updateProject id_ name description createdAt =
    Update
        { uTable = projectTable
        , uUpdateWith =
            const
                ProjectE
                    { projectEId = toFields $ Just id_
                    , projectEName = toFields name
                    , projectEDescription = toFields description
                    , projectECreatedAt = toFields createdAt
                    }
        , uWhere = \row -> projectEId row .== toFields id_
        , uReturning = rReturning projectECreatedAt
        }

deleteProject :: UUID -> Delete Int64
deleteProject id_ =
    Delete
        { dTable = projectTable
        , dWhere = \row -> projectEId row .== toFields id_
        , dReturning = rCount
        }
