{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE LambdaCase #-}

{- |
Module      : OrgAgile.Config
Description : Org Agile App Config Module
License     : GPL-3.0
Maintainer  : Euan Mendoza <bebopbamf@effectfree.dev>
Stability   : experimental
Portability : POSIX

Here is a longer description of this module, containing more
detailed information about what it does.
-}
module OrgAgile.Config (
    DbConfig (..),
    AppLevel (..),
    Config (..),
    configPath,
    readConfig,
    vLogLevel,
) where

import Data.Aeson
import Data.Aeson.Casing
import Data.Yaml.Config
import RIO

data DbConfig = DbConfig
    { dbHost :: Text
    , dbPort :: Int
    , dbUser :: Text
    , dbPass :: Text
    , dbName :: Text
    , dbPool :: Int
    }
    deriving (Eq, Show, Generic)

instance FromJSON DbConfig where
    parseJSON = genericParseJSON $ aesonPrefix snakeCase

data AppLevel
    = Debug
    | Development
    | Production
    deriving (Eq, Show, Generic)

instance FromJSON AppLevel where
    parseJSON = \case
        "debug" -> pure Debug
        "development" -> pure Development
        "production" -> pure Production
        _ -> fail "Invalid app level"

data Config = Config
    { configDbConfig :: DbConfig
    , configPort :: Int
    , configAppLevel :: AppLevel
    }
    deriving (Eq, Show, Generic)

instance FromJSON Config where
    parseJSON = genericParseJSON $ aesonPrefix snakeCase

configPath :: FilePath
configPath = "config.yaml"

readConfig :: IO Config
readConfig = loadYamlSettings [configPath] [] useEnv

vLogLevel :: AppLevel -> LogLevel
vLogLevel Debug = LevelDebug
vLogLevel Development = LevelInfo
vLogLevel Production = LevelError
