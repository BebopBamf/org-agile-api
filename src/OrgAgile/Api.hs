{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

{- |
Module      : OrgAgile.Api
Description : Org Agile Application API Module
License     : GPL-3.0
Maintainer  : Euan Mendoza <bebopbamf@effectfree.dev>
Stability   : experimental
Portability : POSIX

Here is a longer description of this module, containing more
detailed information about what it does.
-}
module OrgAgile.Api (
    API,
    api,
    server,
) where

import OrgAgile.Api.Projects (ProjectsAPI, projectServer)
import OrgAgile.App (AppM)
import Servant (HasServer (ServerT), Proxy (..), type (:>))

type API = "api" :> "v1" :> ProjectsAPI

api :: Proxy API
api = Proxy

server :: ServerT API AppM
server = projectServer
