{-# LANGUAGE FlexibleContexts #-}

{- |
Module      : OrgAgile.Db
Description : Org Agile Database Module
License     : GPL-3.0
Maintainer  : Euan Mendoza <bebopbamf@effectfree.dev>
Stability   : experimental
Portability : POSIX

Here is a longer description of this module, containing more
detailed information about what it does.
-}
module OrgAgile.Db (
    module OrgAgile.Db.Pool,
    runDb,
    selectDb,
    insertDb,
    updateDb,
    deleteDb,
) where

import Data.Pool (withResource)
import Data.Profunctor.Product.Default (Default)
import Database.PostgreSQL.Simple (Connection)
import Opaleye (
    Delete,
    FromFields,
    Select,
    Update,
    runDelete,
    runSelect,
    runUpdate,
 )
import RIO (IO, Monad ((>>=)), MonadIO (liftIO), asks, ($))

import OrgAgile.App (AppM, Env (envDbPool))
import OrgAgile.Db.Pool

runDb :: (Connection -> IO a) -> AppM a
runDb action =
    asks envDbPool >>= \p -> liftIO $ withResource p action

selectDb :: (Default FromFields fields a) => Select fields -> AppM [a]
selectDb a = runDb $ \c -> runSelect c a

insertDb :: (Default FromFields fields a) => Select fields -> AppM [a]
insertDb a = runDb $ \c -> runSelect c a

updateDb :: Update a -> AppM a
updateDb a = runDb $ \conn -> runUpdate conn a

deleteDb :: Delete a -> AppM a
deleteDb a = runDb $ \conn -> runDelete conn a
