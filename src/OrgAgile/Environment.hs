{- |
Module      : OrgAgile.Environment
Description : Org Agile App Environment Module
License     : GPL-3.0
Maintainer  : Euan Mendoza <bebopbamf@effectfree.dev>
Stability   : experimental
Portability : POSIX

Here is a longer description of this module, containing more
detailed information about what it does.
-}
module OrgAgile.Environment (DbPool, HasDbPool (..), getDbPool') where

import OrgAgile.Db.Pool (DbPool)
import RIO

class HasDbPool env where
    getDbPool :: env -> DbPool

getDbPool' :: (HasDbPool env, MonadReader env m) => m DbPool
getDbPool' = ask <&> getDbPool
