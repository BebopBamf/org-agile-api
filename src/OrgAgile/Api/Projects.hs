{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

{- |
Module      : OrgAgile.Api.Projects
Description : Org Agile Application API Module
License     : GPL-3.0
Maintainer  : Euan Mendoza <bebopbamf@effectfree.dev>
Stability   : experimental
Portability : POSIX

Here is a longer description of this module, containing more
detailed information about what it does.
-}
module OrgAgile.Api.Projects (
    ProjectsAPI,
    projectServer,
    projectsAPI,
) where

import Opaleye
import RIO
import Servant

import OrgAgile.App (AppM)
import OrgAgile.Data.Pagination
import OrgAgile.Db (runDb)
import OrgAgile.Db.Schema.Project (Project)
import qualified OrgAgile.Db.Schema.Project as P

type PaginatedProjects = Pagination Project

type ProjectsAPI =
    "projects"
        :> QueryParam "page" Int32
        :> QueryParam "perPage" Int32
        :> Get '[JSON] PaginatedProjects

getProjects :: Maybe Int32 -> Maybe Int32 -> AppM PaginatedProjects
getProjects page perPage = do
    let page' = fromMaybe 1 page
    let perPage' = fromMaybe 5 perPage
    (pagedRes, total') <- runDb $ \c -> do
        r1 <- runSelect c $ P.selectProjectPage page' perPage'
        r2 <- runSelect c P.selectProjectCount :: IO [Int64]
        pure (r1, r2)

    let total = fromIntegral . fromMaybe 0 $ listToMaybe total'
    pure
        $ Pagination
            { paginationData = pagedRes
            , paginationPage = page'
            , paginationPerPage = perPage'
            , paginationTotalPages = total `div` perPage' + 1
            , paginationTotalCount = total
            , paginationLinks =
                PageLinks
                    { pageLinksFirst = "/api/v1/projects?page=1&perPage=" <> tshow perPage'
                    , pageLinksLast = "/api/v1/projects?page=" <> tshow (total `div` perPage' + 1) <> "&perPage=" <> tshow perPage'
                    , pageLinksPrev = if page' > 1 then Just $ "/api/v1/projects?page=" <> tshow (page' - 1) <> "&perPage=" <> tshow perPage' else Nothing
                    , pageLinksNext = if page' < total `div` perPage' then Just $ "/api/v1/projects?page=" <> tshow (page' + 1) <> "&perPage=" <> tshow perPage' else Nothing
                    }
            }

projectServer :: ServerT ProjectsAPI AppM
projectServer = getProjects

projectsAPI :: Proxy ProjectsAPI
projectsAPI = Proxy
