{- |
Module      : OrgAgile.Data
Description : Org Agile Data Types Module
License     : GPL-3.0
Maintainer  : Euan Mendoza <bebopbamf@effectfree.dev>
Stability   : experimental
Portability : POSIX

Here is a longer description of this module, containing more
detailed information about what it does.
-}
module OrgAgile.Data (
    module OrgAgile.Data.Pagination,
) where

import OrgAgile.Data.Pagination
