{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell #-}

{- |
Module      : OrgAgile.Db.Schema.Project
Description : Org Agile Database Project Module
License     : GPL-3.0
Maintainer  : Euan Mendoza <bebopbamf@effectfree.dev>
Stability   : experimental
Portability : POSIX

Here is a longer description of this module, containing more
detailed information about what it does.
-}
module OrgAgile.Db.Schema.Project (
    Project,
    projectTable,
    selectProjects,
    selectProjectPage,
    selectProjectCount,
    selectProjectPageCount,
    selectProjectById,
) where

import Data.Aeson (FromJSON (parseJSON), ToJSON, genericParseJSON, genericToEncoding, genericToJSON)
import Data.Aeson.Casing (aesonPrefix, camelCase)
import Data.Profunctor.Product.TH (makeAdaptorAndInstance)
import Data.Time (LocalTime)
import Data.UUID (UUID)
import Data.Yaml.Aeson (ToJSON (..))
import Opaleye
import RIO

data Project' a b c d = Project
    { projectId :: a
    , projectName :: b
    , projectDescription :: c
    , projectCreatedAt :: d
    }
    deriving (Eq, Show, Generic)

type Project = Project' UUID Text Text LocalTime
type ProjectWrite = Project' (Maybe (Field SqlUuid)) (Field SqlVarcharN) (Maybe (Field SqlText)) (Maybe (Field SqlTimestamp))
type ProjectField = Project' (Field SqlUuid) (Field SqlVarcharN) (Field SqlText) (Field SqlTimestamp)

instance FromJSON Project where
    parseJSON = genericParseJSON $ aesonPrefix camelCase

instance ToJSON Project where
    toJSON = genericToJSON $ aesonPrefix camelCase

    toEncoding = genericToEncoding $ aesonPrefix camelCase

$(makeAdaptorAndInstance "pProject" ''Project')

projectTable :: Table ProjectWrite ProjectField
projectTable =
    table "projects"
        $ pProject
            Project
                { projectId = tableField "id"
                , projectName = tableField "name"
                , projectDescription = tableField "description"
                , projectCreatedAt = tableField "created_at"
                }

-- | Project Queries
selectProjects :: Select ProjectField
selectProjects = selectTable projectTable

selectProjectPage :: Int32 -> Int32 -> Select ProjectField
selectProjectPage page perPage =
    let lim = fromIntegral perPage
        off = fromIntegral $ (page - 1) * perPage
     in limit lim $ offset off $ orderBy (desc projectCreatedAt) selectProjects

selectProjectCount :: Select (Field SqlInt8)
selectProjectCount = countRows selectProjects

selectProjectPageCount :: Int32 -> Int32 -> Select (ProjectField, Field SqlInt8)
selectProjectPageCount page perPage = do
    projects <- selectProjectPage page perPage
    total <- selectProjectCount
    pure (projects, total)

selectProjectById :: UUID -> Select ProjectField
selectProjectById pid = do
    projects <- selectProjects
    where_ (projectId projects .== toFields pid)

    pure projects
