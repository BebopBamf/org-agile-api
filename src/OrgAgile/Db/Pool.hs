{- |
Module      : OrgAgile.Db.Pool
Description : Org Agile Database Pool Module
License     : GPL-3.0
Maintainer  : Euan Mendoza <bebopbamf@effectfree.dev>
Stability   : experimental
Portability : POSIX

Here is a longer description of this module, containing more
detailed information about what it does.
-}
module OrgAgile.Db.Pool (
    DbPool,
    loadPool,
) where

import Data.Pool (Pool, PoolConfig, defaultPoolConfig, newPool)
import Database.PostgreSQL.Simple
import OrgAgile.Config
import RIO
import qualified RIO.Text as T

type DbPool = Pool Connection

toConnInfo :: DbConfig -> ConnectInfo
toConnInfo cfg =
    ConnectInfo
        { connectHost = T.unpack $ dbHost cfg
        , connectPort = fromIntegral $ dbPort cfg
        , connectUser = T.unpack $ dbUser cfg
        , connectPassword = T.unpack $ dbPass cfg
        , connectDatabase = T.unpack $ dbName cfg
        }

fetchConn :: DbConfig -> IO Connection
fetchConn cfg = connect $ toConnInfo cfg

loadPoolCfg :: DbConfig -> PoolConfig Connection
loadPoolCfg cfg = defaultPoolConfig (fetchConn cfg) close 1 (dbPool cfg)

loadPool :: DbConfig -> IO DbPool
loadPool = newPool . loadPoolCfg
