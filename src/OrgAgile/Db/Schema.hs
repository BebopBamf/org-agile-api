{- |
Module      : OrgAgile.Db.Schema
Description : Org Agile Database Schema Module
License     : GPL-3.0
Maintainer  : Euan Mendoza <bebopbamf@effectfree.dev>
Stability   : experimental
Portability : POSIX

Here is a longer description of this module, containing more
detailed information about what it does.
-}
module OrgAgile.Db.Schema (
    module Schema,
) where

import OrgAgile.Db.Schema.Project as Schema
