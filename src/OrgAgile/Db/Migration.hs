{- |
Module      : OrgAgile.Db.Migration
Description : Org Agile Database Migrations Module
License     : GPL-3.0
Maintainer  : Euan Mendoza <bebopbamf@effectfree.dev>
Stability   : experimental
Portability : POSIX

Here is a longer description of this module, containing more
detailed information about what it does.
-}
module OrgAgile.Db.Migration (
    module Database.PostgreSQL.Simple.Migration,
    init,
    migrate,
    validate,
    autoMigrate,
) where

import Database.PostgreSQL.Simple (Connection)
import Database.PostgreSQL.Simple.Migration
import RIO

migrationDir :: FilePath
migrationDir = "migrations"

-- | Init database
init :: Connection -> IO (MigrationResult String)
init conn = liftIO $ runMigration conn defaultOptions MigrationInitialization

-- | Migrate database
migrate :: (MonadIO m) => Connection -> m (MigrationResult String)
migrate conn = liftIO $ runMigration conn defaultOptions (MigrationDirectory migrationDir)

validate :: (MonadIO m) => Connection -> m (MigrationResult String)
validate conn =
    liftIO
        $ runMigrations
            conn
            defaultOptions
            [ MigrationValidation MigrationInitialization
            , MigrationValidation (MigrationDirectory migrationDir)
            ]

-- | Auto Migrations
autoMigrateCommands :: [MigrationCommand]
autoMigrateCommands =
    [ MigrationInitialization
    , MigrationDirectory migrationDir
    , MigrationValidation (MigrationDirectory migrationDir)
    ]

autoMigrate :: (MonadIO m) => Connection -> m (MigrationResult String)
autoMigrate conn = liftIO $ runMigrations conn defaultOptions autoMigrateCommands
