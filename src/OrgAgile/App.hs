{-# LANGUAGE InstanceSigs #-}

{- |
Module      : OrgAgile.App
Description : Org Agile Application Defaults Module
License     : GPL-3.0
Maintainer  : Euan Mendoza <bebopbamf@effectfree.dev>
Stability   : experimental
Portability : POSIX

Here is a longer description of this module, containing more
detailed information about what it does.
-}
module OrgAgile.App (Env (..), AppM, convertApp) where

import Control.Monad.Except (ExceptT (ExceptT))
import OrgAgile.Environment
import RIO
import qualified Servant

-- import qualified Servant as Servant

data Env = Env
    { envDbPool :: DbPool
    , envLogFunc :: LogFunc
    }

type AppM = RIO Env

convertApp :: Env -> AppM a -> Servant.Handler a
convertApp env appM = Servant.Handler $ ExceptT $ try $ runRIO env appM

instance HasDbPool Env where
    getDbPool :: Env -> DbPool
    getDbPool = envDbPool

instance HasLogFunc Env where
    logFuncL :: Lens' Env LogFunc
    logFuncL = lens envLogFunc (\x y -> x{envLogFunc = y})
