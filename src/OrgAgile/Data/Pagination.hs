{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE InstanceSigs #-}

{- |
Module      : OrgAgile.Data.Pagination
Description : Org Agile Data Types Module
License     : GPL-3.0
Maintainer  : Euan Mendoza <bebopbamf@effectfree.dev>
Stability   : experimental
Portability : POSIX

Here is a longer description of this module, containing more
detailed information about what it does.
-}
module OrgAgile.Data.Pagination where

import Data.Aeson
import Data.Aeson.Casing
import Data.Aeson.Types (Parser)
import RIO

data PageLinks = PageLinks
    { pageLinksFirst :: Text
    , pageLinksLast :: Text
    , pageLinksNext :: Maybe Text
    , pageLinksPrev :: Maybe Text
    }
    deriving (Show, Eq, Generic)

instance FromJSON PageLinks where
    parseJSON :: Value -> Parser PageLinks
    parseJSON = genericParseJSON $ aesonPrefix camelCase

instance ToJSON PageLinks where
    toJSON :: PageLinks -> Value
    toJSON = genericToJSON $ aesonPrefix camelCase

    toEncoding :: PageLinks -> Encoding
    toEncoding = genericToEncoding $ aesonPrefix camelCase

data Pagination a = Pagination
    { paginationData :: [a]
    , paginationPage :: Int32
    , paginationPerPage :: Int32
    , paginationTotalPages :: Int32
    , paginationTotalCount :: Int32
    , paginationLinks :: PageLinks
    }
    deriving (Show, Eq, Generic)

-- instance Functor Pagination where
--    fmap :: (a -> b) -> Pagination a -> Pagination b
--    fmap f (Pagination data' count page perPage) = Pagination (fmap f data') count page perPage

instance (FromJSON a) => FromJSON (Pagination a) where
    parseJSON :: Value -> Parser (Pagination a)
    parseJSON = genericParseJSON $ aesonPrefix camelCase

instance (ToJSON a) => ToJSON (Pagination a) where
    toJSON :: Pagination a -> Value
    toJSON = genericToJSON $ aesonPrefix camelCase

    toEncoding :: Pagination a -> Encoding
    toEncoding = genericToEncoding $ aesonPrefix camelCase
