{- |
Module      : OrgAgile
Description : Org Agile Application Module
License     : GPL-3.0
Maintainer  : Euan Mendoza <bebopbamf@effectfree.dev>
Stability   : experimental
Portability : POSIX

Here is a longer description of this module, containing more
detailed information about what it does.
-}
module OrgAgile (Command (..), Options (..), runCommand) where

import Data.Pool (withResource)
import Network.Wai (Middleware)
import qualified Network.Wai.Handler.Warp as Warp
import Network.Wai.Middleware.RequestLogger
import Network.Wai.Middleware.RequestLogger.JSON
import OrgAgile.Api (api, server)
import OrgAgile.App
import OrgAgile.Config
import OrgAgile.Db
import OrgAgile.Db.Migration as Migration
import RIO
import Servant

data Command
    = Init ()
    | Migrate ()
    | Run ()

-- | Command line arguments
data Options = Options
    { optionsVerbose :: !Bool
    , optionsAutoMigrate :: !Bool
    }

initDb :: AppM ()
initDb = do
    logInfo "Initializing Database"
    res <- runDb Migration.init
    case res of
        Migration.MigrationSuccess -> logInfo "Database initialized"
        Migration.MigrationError err -> logError $ "Database initialization failed: " <> fromString err

migrateDb :: AppM ()
migrateDb = do
    logInfo "Migrating Database"
    res <- runDb Migration.migrate
    case res of
        Migration.MigrationSuccess -> logInfo "Database migrated"
        Migration.MigrationError err -> logError $ "Database migration failed: " <> fromString err

autoMigrate' :: DbPool -> IO ()
autoMigrate' pool = do
    _ <- withResource pool Migration.autoMigrate
    pure ()

validate' :: DbPool -> IO ()
validate' pool = do
    _ <- withResource pool Migration.validate
    pure ()

mkApp :: Env -> Application
mkApp env =
    serve api hoistedServer
  where
    hoistedServer = hoistServer api (convertApp env) server

jsonRequestLogger :: IO Middleware
jsonRequestLogger =
    mkRequestLogger
        $ defaultRequestLoggerSettings
            { outputFormat = CustomOutputFormatWithDetails formatAsJSON
            }

runApp :: Int -> Env -> IO ()
runApp port env = do
    warpLogger <- jsonRequestLogger
    let warpSettings =
            Warp.defaultSettings
                & Warp.setPort port
                & Warp.setTimeout 60
    Warp.runSettings warpSettings $ warpLogger $ mkApp env

runCommand :: Options -> Command -> Config -> IO ()
runCommand opts cmd cfg = do
    pool <- loadPool $ configDbConfig cfg

    lo' <- logOptionsHandle stderr (optionsVerbose opts)
    let lo = setLogMinLevel (vLogLevel . configAppLevel $ cfg) lo'

    withLogFunc lo $ \lf ->
        let env =
                Env
                    { envDbPool = pool
                    , envLogFunc = lf
                    }
         in case cmd of
                Init _ -> runRIO env initDb
                Migrate _ -> runRIO env migrateDb
                Run _ -> do
                    if optionsAutoMigrate opts
                        then autoMigrate' pool
                        else validate' pool
                    runApp (configPort cfg) env
