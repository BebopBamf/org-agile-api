#!/bin/sh

# Wait for Postgres to start
if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi

ls -lA migrations

# Run migrations
todo-server migrate

exec "$@"
