DROP VIEW IF EXISTS v_issues;
DROP TABLE IF EXISTS issues;
DROP TYPE IF EXISTS t_issue_type;
DROP TYPE IF EXISTS t_issue_status;
DROP TABLE IF EXISTS projects;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS projects (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    name varchar (63) NOT NULL,
    description text NOT NULL DEFAULT '',
    created_at timestamp NOT NULL DEFAULT now()
);

CREATE TYPE t_issue_status AS ENUM ('todo', 'in-progress', 'completed');
CREATE TYPE t_issue_type AS ENUM ('epic', 'story', 'task', 'issue');

CREATE TABLE IF NOT EXISTS issues (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    project_id uuid NOT NULL REFERENCES projects (id) ON DELETE CASCADE,
    title varchar (255) NOT NULL,
    description text NOT NULL DEFAULT '',
    type t_issue_type NOT NULL DEFAULT 'story',
    status t_issue_status NOT NULL DEFAULT 'todo',
    created_at timestamp NOT NULL DEFAULT now()
);

CREATE VIEW v_issues
    AS SELECT *, row_number() OVER (
                  PARTITION BY project_id
                  ORDER BY created_at
        ) AS issue_num
    FROM issues;

INSERT INTO projects (id, name)
VALUES ('0f0c8b59-f0f3-4138-8c35-753112583883', 'Test Project 01'),
       ('4c30ef6d-0405-47aa-bc64-23d04cbd2e13', 'Test Project 02');

INSERT INTO issues (project_id, title, description)
VALUES ('0f0c8b59-f0f3-4138-8c35-753112583883', 'Story 01', ''),
       ('0f0c8b59-f0f3-4138-8c35-753112583883', 'Story 02', ''),
       ('0f0c8b59-f0f3-4138-8c35-753112583883', 'Story 03', ''),
       ('0f0c8b59-f0f3-4138-8c35-753112583883', 'Story 04', ''),
       ('0f0c8b59-f0f3-4138-8c35-753112583883', 'Story 05', ''),
       ('4c30ef6d-0405-47aa-bc64-23d04cbd2e13', 'P2 Story 01', ''),
       ('4c30ef6d-0405-47aa-bc64-23d04cbd2e13', 'P2 Story 01', ''),
       ('4c30ef6d-0405-47aa-bc64-23d04cbd2e13', 'P2 Story 01', ''),
       ('4c30ef6d-0405-47aa-bc64-23d04cbd2e13', 'P2 Story 01', ''),
       ('4c30ef6d-0405-47aa-bc64-23d04cbd2e13', 'P2 Story 01', ''),
       ('4c30ef6d-0405-47aa-bc64-23d04cbd2e13', 'P2 Story 01', '');
